//
//  RateMyApp.swift
//  EngRead
//
//  Created by Ernie Cho on 9/20/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import Foundation
import StoreKit
import UIKit
class RateMyApp: UIViewController {
    let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if launchedBefore  {
            print("Not first launch.")
        } else {
            if #available(iOS 10.3, *){
                SKStoreReviewController.requestReview()
            }
            print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
    }
    
    @IBAction func rateappButton(_ sender: UIButton) {
        if #available(iOS 10.3, *){
            SKStoreReviewController.requestReview()
        }
    }
}

