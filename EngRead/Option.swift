//
//  Option.swift
//  EngRead
//
//  Created by Ernie Cho on 6/18/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class OptionUI: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    
    //Variabes for IU Properties.
    @IBOutlet weak var varVolume: UISlider!
    @IBOutlet weak var varpitchMultiplier: UISlider!
    @IBOutlet weak var varvoiceRate: UISlider!
    @IBOutlet weak var langpickerView: UIPickerView!
    @IBOutlet weak var NotificationSwitch: UISwitch!
    
    
    var Language: String!
    let langs = ["English (United States)","English (Australia)","English (Ireland)","English (South Africa)","English (United Kingdom)"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        varVolume.value = self.LoadOption().volume
        varpitchMultiplier.value = self.LoadOption().multi
        varvoiceRate.value = self.LoadOption().rate
        Language = self.LoadOption().lang
        NotificationSwitch.isOn = self.LoadOption().notif
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return langs.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return langs[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            Language = "en-US"
            break
        case 1:
            Language = "en-AU"
            break
        case 2:
            Language = "en-IE"
            break
        case 3:
            Language = "en-ZA"
            break
        case 4:
            Language = "en-GB"
            break
        default:
            Language = "en-US"
        }
    }
    
    //Action Button
    @IBAction func applysettingsButton(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Option changes will take affect.", message: "New settings will be applied immediatly to application.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.SaveOption(volume: varVolume.value, multi: varpitchMultiplier.value, rate: varvoiceRate.value, lang: Language, notif: NotificationSwitch.isOn)
    }
    @IBAction func ResetOption(_ sender: UIButton) {
        varVolume.value = 1
        varpitchMultiplier.value = 1
        varvoiceRate.value = 0.5
        NotificationSwitch.isOn = true
        Language = "en-US"
        self.SaveOption(volume: varVolume.value, multi: varpitchMultiplier.value, rate: varvoiceRate.value, lang: Language, notif: NotificationSwitch.isOn)
    }
    
    func SaveOption(volume: Float, multi: Float, rate: Float, lang: String, notif: Bool) {
        UserDefaults.standard.set(volume, forKey: "volumeOption")
        UserDefaults.standard.set(multi, forKey: "multiOption")
        UserDefaults.standard.set(rate, forKey: "rateOption")
        UserDefaults.standard.set(lang, forKey: "LangOption")
        UserDefaults.standard.set(notif, forKey: "NotifyOption")
        UserDefaults.standard.set(true, forKey: "SavedOption")
        UserDefaults.standard.synchronize()
    }
    
    func LoadOption() -> (volume: Float, multi: Float, rate: Float, lang: String, notif: Bool) {
        var valuevolume: Float
        var valuemulti: Float
        var valuerate: Float
        var language: String
        var valnotification: Bool
        let Saved = UserDefaults.standard.bool(forKey: "SavedOption")
        if Saved {
            valuevolume = UserDefaults.standard.float(forKey: "volumeOption")
            valuemulti = UserDefaults.standard.float(forKey: "multiOption")
            valuerate = UserDefaults.standard.float(forKey: "rateOption")
            valnotification = UserDefaults.standard.bool(forKey: "NotifyOption")
            language = UserDefaults.standard.string(forKey: "LangOption")!
        } else {
            valuevolume = 1
            valuemulti = 1
            valuerate = 0.5
            valnotification = true
            language = "en-US"
        }
        return (valuevolume, valuemulti, valuerate, language, valnotification)
    }
}
