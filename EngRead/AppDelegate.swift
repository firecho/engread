//
//  AppDelegate.swift
//  EngRead
//
//  Created by Ernie Cho on 4/10/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import UIKit
import Firebase
import StoreKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let GADConfigString: String = "ca-app-pub-3777362353172933~5960749565"
    var UseCounter: Int = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Use Firebase library to configure APIs.
        FirebaseApp.configure()
        
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.configure(withApplicationID: GADConfigString)
        
        //Load Notification Check
        self.Notifcation()
        
        //Check How many times use before Review App
        UseCounter = UserDefaults.standard.integer(forKey: "UseLaunchCounter")
        if UseCounter == 7 {
            UseCounter = 0
            UserDefaults.standard.set(UseCounter, forKey: "UseLaunchCounter")
            if #available(iOS 10.3, *){
                SKStoreReviewController.requestReview()
            }
        } else {
            UseCounter += 1
            UserDefaults.standard.set(UseCounter, forKey: "UseLaunchCounter")
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func Notifcation () {
        //registration
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {( grant, error ) in }
        //Load Variable
        let content = UNMutableNotificationContent()
        content.title = "Learn English X Notification"
        content.body = "Notify to continue to learn English Language."
        content.sound = UNNotificationSound.default()
        var date = DateComponents()
        date.hour = 10
        date.minute = 00
        //Check Notification is Turn on Control.
        if OptionUI().LoadOption().notif == true {
            // Deliver the notification at 10 am morning.
            let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: true)
            let request = UNNotificationRequest(identifier: "com.ernshu.EngRead", content: content, trigger: trigger)
            // Schedule the notification.
            let center = UNUserNotificationCenter.current()
            center.add(request) { (error : Error?) in
                if error != nil {
                    // Handle any errors
                }
            }
            
        } else {
            print("Notification Turn off")
            let center = UNUserNotificationCenter.current()
            center.removeAllDeliveredNotifications()
        }
    } //end of notification method
}

